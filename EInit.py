"""
This file is part of code_stage_l3.

code_stage_l3 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

code_stage_l3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with code_stage_l3.  If not, see <https://www.gnu.org/licenses/>.
"""

from math import *
import numpy 
from nglobal import n

# On définit ici la fonction qui produit le vecteur formé de la répartition initiale du champ élétrique 
def EInit():
	# On met n en globale  
	global n 

	# Déclaration de E 
	E = numpy.zeros([2,n]) 
	
	# On construit le vecteur 
	for i in numpy.arange(0, n, 1):
		# On remplace dans la formule suivante la fonction qu'on veut pour les valeurs initiales de E
		E[0,i] = 0
		#E[1,i] = exp((-(i-250)**2)/3000)*sin(-0.3*i)
		E[1,i] = 0
		
	return E


def Esecond():
	# On met n en globale  
	global n 

	# Déclaration de E 
	E = numpy.zeros([2,n]) 
	
	# On construit le vecteur 
	for i in numpy.arange(0, n, 1):
		# On remplace dans la formule suivante la fonction qu'on veut pour les valeurs initiales de E
		E[0,i] = 0
		#E[1,i] = exp((-(i-250)**2)/3000)*sin(-0.3*i)
		E[1,i] = 0
		
	return E
